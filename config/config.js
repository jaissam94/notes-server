let currEnv = 'testing';

function configFunction() {
    let config = {};
    switch(currEnv) {
        case `testing`: 
            config = require('./env/testing.json');
            break;
        case `production`:
            config = require('./env/production.json');
            break;
        case `default`:
            console.log(`error`);
            break;    
    }
    return config;
}

module.exports = configFunction;