var responseHandler = require('../common-functions/response-handler');
var responseCode = require('../helper/response-code.json');
var responseMessage = require('../helper/response-message.json');
var noteModel = require('../models/note-model');

var addNote = async (req, res) => {
    if(!(req.body.title && req.body.description)) {
        responseHandler.responseWithoutData(res, responseCode.PartialContent, responseMessage.PartialContent);
    } else {
        var note = new noteModel({title: req.body.title, description: req.body.description})
        try {
            let saved = await note.save();
            console.log('saved ==>> ', saved );
            responseHandler.responseWithData(res, responseCode.Created, responseMessage.Added, saved);
        } catch(error) {
            responseHandler.responseWithoutData(res, responseCode.InternalServerError, responseMessage.InternalServerError)
        }
        
        
    }
}

var notesList = async (req, res) => {
    try {
        let allNotes = await noteModel.find({})
        
        // let allNotes = await noteModel.paginate({}, {page: req.body.page, limit: req.body.limit})
        console.log('allNotes ===>> ', allNotes);
        responseHandler.responseWithData(res, responseCode.OK, responseMessage.OK, allNotes)
    } catch(err) {
        console.log('err note list ==>> ', err)
        responseHandler.responseWithoutData(res, responseCode.InternalServerError, responseMessage.InternalServerError)
    }
}

var noteDetail = (req, res) =>  {
    
}

var deleteNote = async (req, res) => {
    console.log('params =>> ', req.params)
    if(!req.params._id)
        responseHandler.responseWithoutData(res, responseCode.InternalServerError, responseMessage.InternalServerError)
    else {
        
        noteModel.findOneAndDelete({_id: req.params._id}).then(response => {
        responseHandler.responseWithoutData(res, responseCode.Deleted, responseMessage.Deleted)
        }, error => {
        //  console.log('err ')
        responseHandler.responseWithoutData(res, responseCode.InternalServerError, responseMessage.InternalServerError)
        })       
    }
}

var updateNote = async (req, res) => {
    if(!(req.body._id && req.body.title && req.body.description)) {
        responseHandler.responseWithoutData(res, responseCode.InternalServerError, responseMessage.InternalServerError)
    } else  {
        let query = {
            $set: {
                title: req.body.title,
                description: req.body.description
            }
        }
        try {
            let data = await noteModel.findOneAndUpdate({_id: req.body._id}, query, {new: true})
            console.log('data ==>> ', data)
            responseHandler.responseWithData(res, responseCode.OK, responseMessage.Updated, data);
        } catch(error) {
            responseHandler.responseWithoutData(res, responseCode.InternalServerError, responseMessage.InternalServerError)
        }
         
    }
}

var searchNote = async (req, res) => {
    try {
        /* let searchList = await noteModel.find({
            description: {
                $regex: new RegExp(req.params.search)
            }
        }) */
        let searchList = await noteModel.find({
            $or: [  {
                        description: {
                                        $regex: new RegExp(req.params.search) 
                                    }
                    }, 
                    {
                        title: {
                            $regex: new RegExp(req.params.search)
                        }
                    }]
        })
        responseHandler.responseWithData(res, responseCode.OK, responseMessage.OK, searchList);
    } catch(error) {
        responseHandler.responseWithoutData(res, responseCode.InternalServerError, responseMessage.InternalServerError)
    }
    
    // console.log('searchList ==>> ', searchList)
}

module.exports = {
    addNote: addNote,
    notesList: notesList,
    noteDetail: noteDetail,
    deleteNote: deleteNote,
    updateNote: updateNote,
    searchNote: searchNote
} 