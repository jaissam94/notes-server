var responseWithData = (res, code, message, data) => {
    res.send({code: code, message: message, data: data});
}

var responseWithoutData = (res, code, message) => {
    res.send({code: code, message: message})
}
module.exports = {
    responseWithData: responseWithData,
    responseWithoutData: responseWithoutData
}