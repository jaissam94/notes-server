const express = require('express');
const app = express();
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require(`body-parser`);
var config = require('./config/config')();

/*********** Mongoose Connection with db  ********************/
mongoose.connect(`mongodb://localhost/notes-crud`,  { useNewUrlParser: true } );
let db = mongoose.connection;
db.on('error', ()=> {
    console.log(`Error while connecting with mongo db`);
    setTimeout(() => {
        mongoose.connect(`mongodb://localhost/notes-crud`, {useNewUrlParser: true});
    }, 2000)
})
db.on('open', () => {
    console.log(`Connected with mongo db`);
})
/*********** End Mongoose Connection with db  ********************/

app.use(cors());


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({limit: '50mb'}));

app.use('/api/notes', require('./routes/note-route'));

let port = app.listen(config.port, () => {
    console.log(`Server running on port ${port.address().port}`);
})