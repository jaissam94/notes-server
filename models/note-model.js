var mongoose = require('mongoose');
// var mongoosePaginate = require('mongoose-paginate');
var noteSchema = new mongoose.Schema({
    title: {
        type: String,
        trim: true
    },
    description: {
        type: String,
        trim: true
    }
}, {timestamps: {}})
// noteSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('note-schema', noteSchema)