var express = require('express');
var router = express.Router();
var noteController = require('../webservices/note-controller');
router.get('/notes-list', noteController.notesList);
router.post('/add-note', noteController.addNote);
router.get('/delete-note/:_id', noteController.deleteNote);
router.post('/note-detail', noteController.noteDetail);
router.post('/update-note', noteController.updateNote);
router.get('/search-note/:search', noteController.searchNote)

module.exports = router;